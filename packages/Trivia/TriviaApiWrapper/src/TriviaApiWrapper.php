<?php

namespace Trivia\TriviaApiWrapper;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class TriviaApiWrapper
{

    protected $client;

    public function __construct() {

        $base_uri = config('trivia.app_url');

        $this->client = new Client(
            array(
                'base_uri' => $base_uri,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Accept'       => 'application/json'
                )
            )
        );

    }

    public function getRandom($count=1) {

        $result = $this->client->request(
            'GET',
            '/api/random',
            array(
                'json' => array(
                    'count' => $count
                )
            )
        );

        return $result->getBody()->getContents();

    }

    public function getClues($category) {

        $result = $this->client->request(
            'GET',
            '/api/clues',
            array(
                'json' => array(
                    'category' => $category
                )
            )
        );

        return $result->getBody()->getContents();

    }

    public function getCategories($count=1) {

        $result = $this->client->request(
            'GET',
            '/api/categories',
            array(
                'json' => array(
                    'count' => $count
                )
            )
        );

        return $result->getBody()->getContents();

    }

    public function getCategory($id) {

        $result = $this->client->request(
            'GET',
            '/api/category?id='.$id
        );

        return $result->getBody()->getContents();

    }

    public function invalid($id) {

        $result = $this->client->request(
            'POST',
            '/api/invalid',
            array(
                'json' => array(
                    'id' => $id
                )
            )
        );

        return $result->getBody()->getContents();

    }
}