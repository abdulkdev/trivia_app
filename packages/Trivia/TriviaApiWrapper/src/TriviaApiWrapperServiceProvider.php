<?php

namespace Trivia\TriviaApiWrapper;

use Illuminate\Support\ServiceProvider;

class TriviaApiWrapperServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/triviaapiwrapper.php', 'triviaapiwrapper');

        // Register the service the package provides.
        $this->app->singleton('triviaapiwrapper', function ($app) {
            return new Triviaapiwrapper;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['triviaapiwrapper'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/triviaapiwrapper.php' => config_path('triviaapiwrapper.php'),
        ], 'triviaapiwrapper.config');

    }
}
