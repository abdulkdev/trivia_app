<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Trivia\TriviaApiWrapper\TriviaApiWrapper;
use Validator;

class ApiController extends Controller{

    public function __construct() {

        $this->client = new TriviaApiWrapper();

    }

    public function index(Request $request) {
        $getRandom =  $this->client->getRandom($request->count);
        return response($getRandom);
    }

    public function getClues(Request $request) {
        $getClues =  $this->client->getClues($request->category);
        return response($getClues);
    }

    public function getCategories(Request $request) {
        $getCategories =  $this->client->getCategories($request->count);
        return response($getCategories);
    }

    public function getCategory(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return 'The id field is required.';
        }
        $getCategory =  $this->client->getCategory($request->id);
        return response($getCategory);
    }

    public function invalid(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return 'The id field is required.';
        }
        $invalid =  $this->client->invalid($request->id);
        return response($invalid);
    }
}