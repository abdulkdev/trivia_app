import Vue from 'vue';
import moment from 'moment'
// Filter to format date
Vue.filter('formatDate', function(date) {
    if (date) {
        return moment(String(date)).format('MM/DD/YYYY hh:mm')
    }
});
