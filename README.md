# Installations
  - Install nodejs
  - Install yarn
  - Install composer

# Clone the repo!
```
$ cd trivia_app
```

# Execute the commands for installing modules
  - composer install
  - yarn install

# Execute the commands for installing modules
  - yarn dev (This command is used to run vue application)
  - php artisan serve (This command is used to run laravel application)

# Run this URL in the browser
  - open localhost:8000 in browser
 



