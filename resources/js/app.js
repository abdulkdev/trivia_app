
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

import './bootstrap.js';
import Vue from 'vue';

//importing library for material UI components of vue
import Vuetify from 'vuetify';
Vue.use(Vuetify);

//Routes
import Routes from '@/js/routes.js';

//Filters
import './filters';

//components

import App from '@/js/views/App';

//Intializing app
const app = new Vue({
    el:"#app",
    router:Routes,
    render: h => h(App),
});

export default app;
