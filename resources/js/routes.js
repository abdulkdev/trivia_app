import Vue from 'vue';
import VueRouter from 'vue-router';

import Dashboard from '@/js/components/Dashboard';
import Category from '@/js/components/Category';
import Question from '@/js/components/Question';


Vue.use(VueRouter);
// Applications routes
const router = new VueRouter({
    made:'history',
    routes:[
        {
            path:'/dashboard',
            name:'dashboard',
            component: Dashboard
        },
        {
            path:'/categories',
            name:'categories',
            component: Category
        },
        {
            path:'/questions/:category?',
            name:'questions',
            component: Question
        },
        { path: '', redirect: '/dashboard' }
    ]
});

export default router;
