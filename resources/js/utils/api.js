//Library to make http calls
import axios from 'axios';

import { API_BASE_URL, DEFAULT_COUNT } from '../config';

//API call to get categories list
export const getCategories = (count=DEFAULT_COUNT) =>
    axios.get(`${API_BASE_URL}/categories?count=${count}`);
//API call to get questions list
export const getQuestions = (url, count=DEFAULT_COUNT) =>
    axios.get(`${API_BASE_URL}/${url}count=${count}`);

//API call to get mark invalid
export const markAsInvalid = (clue) =>
    axios.post(`${API_BASE_URL}/invalid`,clue);
