<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });


    Route::get('/random', 'ApiController@index');
    Route::get('/clues', 'ApiController@getClues');
    Route::get('/categories', 'ApiController@getCategories');
    Route::get('/category', 'ApiController@getCategory');
    Route::post('/invalid', 'ApiController@invalid');
